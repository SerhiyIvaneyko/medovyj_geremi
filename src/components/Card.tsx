import React from 'react';

type Props = {
  card: any;
}

const Card:React.FC<Props> = (props) => {
  const { card } = props;
  return (
    <div className="card | shadow-sm">
      <h2 className="card-suptitle">0{card.id}</h2>
      <img src={card.imageSrc} className="card-img-top" alt=""/>
      <div className="card-body">
        <h5 className="card-title">{card.title}</h5>
        <p className="card-text">Some quick example text to build on the card title and make up the bulk of the card's content.</p>
        <button className="btn btn-primary btn-lg">Go somewhere</button>
      </div>
    </div>
  )
}

export default Card;
