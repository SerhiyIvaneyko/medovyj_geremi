import React from 'react';
import MedovyjGeremiImage from 'assets/images/medovyj_geremi.png';

type Props = {}

const Footer:React.FC<Props> = () => {
  const currentYear = new Date().getFullYear();
  return (
    <footer className="bg-gray-900 py-8 md:py-16">
      <div className="text-white text-center">
        <div className="bg-white rounded-full w-56 h-56 mx-auto mb-2 shadow-md overflow-hidden">
          <img className="w-full h-full object-cover" src={MedovyjGeremiImage} alt="Медовий Єремій" />
        </div>
        <span className="block font-body2 text-2xl mb-4">Медовий Єремій</span>
        <small className="">&copy; {currentYear}</small>
      </div>
    </footer>
  )
}

export default Footer;
