import React from 'react';
import { Link } from 'react-router-dom';

type Props = {}

const links = [
  { id: 1, to: '/about-us', label: 'Про Нас' },
  { id: 2, to: '/technology', label: 'Технології' },
  { id: 3, to: '/products', label: 'Продукція' },
  { id: 4, to: '/testimonials', label: 'Відгуки' },
  { id: 5, to: '/contacts', label: 'Контакти' },
]

const Header:React.FC<Props> = () => {
  const [ toggler, setToggler ] = React.useState<boolean>(false);
  const [ visible, setVisible ]= React.useState<boolean>(false);

  React.useEffect(() => {
    // On init check is lower/higher than screen height
    handleScroll();
    window.addEventListener('scroll', handleScroll);
    return () => window.removeEventListener('scroll', handleScroll);
    // eslint-disable-next-line
  }, []);

  const handleScroll = () => {
    setVisible(window.scrollY >= window.innerHeight);
  }

  return (
    <header className={`${ visible ? 'fixed bg-white shadow' : 'absolute bg-transparent' } top-0 left-0 w-full opacity-90 py-4 z-50`}>
      <div className="container flex items-center">
        <Link to="/" className={`text-2xl md:text-3xl lg:text-4xl ${ visible ? 'text-primary' : 'text-white' } font-bold font-body2`}>Медовий Єремій</Link>
        <span className="flex-grow"></span>
        <nav className="hidden md:flex space-x-6">
          {links.map(link => (
            <Link
              key={`nav-item-${link.id}`}
              to={link.to}
              className={`
                ${ visible ? 'text-primary hover:text-primary-dark' : 'text-white hover:text-gray-50' }
                text-sm font-bold
              `}
            >{link.label}</Link>
          ))}
        </nav>
      </div>
    </header>
  )
}

export default Header;
