import { BrowserRouter as Router, Switch, Route } from 'react-router-dom';
// Layouts
import Header from 'layouts/Header';
import Footer from 'layouts/Footer';
// Pages
import HomePage from 'pages/HomePage';

const App = () => {
  return (
    <Router>
      <Header />
      <Switch>
        <Route exact path="/" component={HomePage} />
      </Switch>
      {/* <Footer /> */}
    </Router>
  );
}

export default App;
