import React from 'react';
import MedovyjGeremiImage from 'assets/images/medovyj_geremi.png';

type Props = {};

const HeroSection:React.FC<Props> = () => {
  return (
    <div className="relative bg-gradient-to-b from-primary-light to-primary py-16 lg:py-32 flex items-center min-h-screen">
      <div className="container md:w-1/2 xl:w-2/5 2xl:w-1/3 text-white text-center">
        <div className="bg-white rounded-full w-48 h-48 md:w-56 md:h-56 lg:w-96 lg:h-96 mx-auto mb-4 md:mb-8 shadow-md overflow-hidden">
          <img className="w-full h-full object-cover" src={MedovyjGeremiImage} alt="Медовий Єремій" />
        </div>
        <h1 className="text-2xl lg:text-4xl font-bold mb-4">Вас вітає, <span className="block font-body2 text-4xl lg:text-6xl">Медовий Єремій</span></h1>
        <p className="text-lg mb-4 md:mb-8">Наймедовіший хлопчик України. В якого найсмачніший мед та корисна медова продукція</p>
        <button
          className="
            bg-white hover:bg-gray-50 rounded-lg
            text-primary text-md font-bold
            py-4 px-8"
          type="button"
        >Детальніше</button>
      </div>
    </div>
  )
}

export default HeroSection;
