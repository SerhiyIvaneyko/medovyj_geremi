import React from 'react';
// Sections
import HeroSection from './HeroSection';
import AboutUsSection from './AboutUsSection';
import TechnologySection from './TechnologySection';
import ProductSection from './ProductSection';
import TestimonialSection from './TestimonialSection';
import ContactUsSection from './ContactUsSection';

type Props = {};

const HomePage:React.FC<Props> = () => {
  return (
    <main>
      {/* Hero section */}
      <HeroSection />
      {/* About Us */}
      <AboutUsSection />
      {/* Technology */}
      {/* <TechnologySection /> */}
      {/* Products */}
      <ProductSection />
      {/* Testimonial */}
      <TestimonialSection />
      {/* Contact Us */}
      <ContactUsSection />
    </main>
  )
}

export default HomePage;
