import 'swiper/swiper.min.css';
import 'swiper/components/pagination/pagination.min.css';

import React from 'react';
import SwiperCore, { Autoplay, Pagination } from 'swiper';
import { Swiper, SwiperSlide } from 'swiper/react';

SwiperCore.use([Pagination, Autoplay]);

type Props = {}

const testimonials = [
  { id: 1, name: 'Leanne Graham', imageUrl: 'http://placekitten.com/300/300', text: 'Lorem ipsum, dolor sit amet consectetur adipisicing elit. Totam, optio. Nesciunt ipsam sequi maxime dolor cumque in necessitatibus fugit modi. Ut odit quae totam veniam labore rerum consequatur harum architecto!' },
  { id: 2, name: 'Ervin Howell', imageUrl: 'http://placekitten.com/400/400', text: 'Lorem ipsum, dolor sit amet consectetur adipisicing elit. Totam, optio. Nesciunt ipsam sequi maxime dolor cumque in necessitatibus fugit modi. Ut odit quae totam veniam labore rerum consequatur harum architecto!' },
  { id: 3, name: 'Clementine Bauch', imageUrl: 'http://placekitten.com/500/500', text: 'Lorem ipsum, dolor sit amet consectetur adipisicing elit. Totam, optio. Nesciunt ipsam sequi maxime dolor cumque in necessitatibus fugit modi. Ut odit quae totam veniam labore rerum consequatur harum architecto!' },
  { id: 4, name: 'Patricia Lebsack', imageUrl: 'http://placekitten.com/600/600', text: 'Lorem ipsum, dolor sit amet consectetur adipisicing elit. Totam, optio. Nesciunt ipsam sequi maxime dolor cumque in necessitatibus fugit modi. Ut odit quae totam veniam labore rerum consequatur harum architecto!' }
]

const TestimonialSection:React.FC<Props> = () => {
  return (
    <div className="relative py-16 md:py-32 z-10">
      <div className="absolute top-0 left-0 w-full h-full"></div>
      <div className="relative z-20">
        <p className="text-primary text-sm font-bold text-center uppercase">Lorem ipsum dolor sit</p>
        <h2 className="text-4xl font-bold text-center mb-4">Що кажуть людоньки</h2>

        <div className="container md:w-2/3 lg:w-1/2">
          <Swiper slidesPerView={1} pagination={{ clickable: true }} autoplay={{ delay: 5000 }} loop>
            {testimonials.map(testimonial => (
              <SwiperSlide key={`testimonial-item-${testimonial.id}`}>
                <div className="flex flex-col lg:flex-row lg:items-center pb-16">
                  <div className="flex-shrink-0 w-56 h-56 rounded-full overflow-hidden mb-4 mx-auto lg:mb-0">
                    <img className="w-full h-full object-cover" src={testimonial.imageUrl} alt={testimonial.name} />
                  </div>
                  <div className="lg:pl-8 text-center lg:text-left">
                    <h5 className="font-bold text-xl">{testimonial.name}</h5>
                    <p className="block font-light">{testimonial.text}</p>
                  </div>
                </div>
              </SwiperSlide>
            ))}
          </Swiper>
        </div>
      </div>
    </div>
  )
}

export default TestimonialSection;
