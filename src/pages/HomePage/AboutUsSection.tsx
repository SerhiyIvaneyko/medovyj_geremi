import React from 'react';
import AboutUsImage1 from 'assets/images/about_us/about_1.jpg';
import AboutUsImage2 from 'assets/images/about_us/about_2.jpg';
import AboutUsImage3 from 'assets/images/about_us/about_4.jpg';

type Props = {};

const technologies = [
  { id: 1, title: 'Технологія', text: 'Lorem ipsum dolor sit, amet consectetur adipisicing elit. A ipsum saepe ipsam. Veniam ea velit dolores sit aliquid expedita, maiores iure ducimus vitae culpa ipsam possimus. Consequuntur provident quis dolorum.' },
  { id: 2, title: 'Технологія', text: 'Lorem ipsum dolor sit, amet consectetur adipisicing elit. A ipsum saepe ipsam. Veniam ea velit dolores sit aliquid expedita, maiores iure ducimus vitae culpa ipsam possimus. Consequuntur provident quis dolorum.' },
  { id: 3, title: 'Технологія', text: 'Lorem ipsum dolor sit, amet consectetur adipisicing elit. A ipsum saepe ipsam. Veniam ea velit dolores sit aliquid expedita, maiores iure ducimus vitae culpa ipsam possimus. Consequuntur provident quis dolorum.' }
]

const AboutUsSection:React.FC<Props> = () => {
  return (
    <div className="relative py-16 lg:py-32 z-10">
      <div className="container 2xl:w-4/5">
        <div className="flex flex-wrap -mx-4">
          <div className="w-full lg:w-1/2 lg:order-2 px-4 mb-4">

            <div className="flex -mx-2 flex-row items-center">
              <div className="hidden lg:block lg:w-2/5 px-2">
                <div className="relative bg-white shadow rounded-lg pt-48 overflow-hidden">
                  <img className="absolute top-0 left-0 w-full h-full object-cover" src={AboutUsImage2} alt="" />
                </div>
              </div>
              <div className="w-full lg:w-3/5 px-2">
                <div className="relative bg-white shadow rounded-lg pt-80 overflow-hidden mb-4">
                  <img className="absolute top-0 left-0 w-full h-full object-cover" src={AboutUsImage1} alt="" />
                </div>
                <div className="hidden lg:block lg:w-4/5 relative bg-white shadow rounded-lg pt-56 overflow-hidden">
                  <img className="absolute top-0 left-0 w-full h-full object-cover" src={AboutUsImage3} alt="" />
                </div>
              </div>
            </div>

          </div>
          <div className="w-full lg:w-1/2 lg:order-1 px-4">
            <p className="text-sm text-primary font-bold uppercase">Lorem ipsum dolor sit</p>
            <h2 className="text-4xl font-bold mb-4">Дещо про нас</h2>
            <p className="font-light text-lg mb-8">Lorem ipsum dolor sit, amet consectetur adipisicing elit. Officiis, rem dolore culpa delectus tenetur sunt, consequuntur soluta facere ab eos excepturi distinctio doloribus dicta beatae dolores labore perferendis pariatur modi aspernatur exercitationem accusantium totam? Doloribus expedita placeat iste laudantium distinctio?</p>

            <div>
              {technologies.map((technology, index) => (
                <div key={`technology-item-${technology.id}`} className={`relative xl:w-2/3 pt-8 pl-8 lg:ml-${index}0`}>
                  <span className="absolute left-0 top-0 text-primary text-8xl font-extrabold opacity-80">{technology.id}</span>
                  <div className="relative z-10">
                    <h3 className="text-xl font-bold">{technology.title} {technology.id}</h3>
                    <p className="text-lg font-light">{technology.text}</p>
                  </div>
                </div>
              ))}
            </div>
          </div>
        </div>
      </div>
    </div>
  )
}

export default AboutUsSection;
