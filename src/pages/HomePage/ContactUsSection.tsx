import React from 'react';
import { ReactComponent as FacebookSVG } from 'assets/images/facebook.svg';
import { ReactComponent as InstagramSVG } from 'assets/images/instagram.svg';
import { ReactComponent as TwitterSVG } from 'assets/images/twitter.svg';

type Props = {};

const ContactUsSection:React.FC<Props> = () => {
  return (
    <div className="relative bg-gradient-to-b from-primary to-primary-light opacity-80 py-16 md:py-32 z-10">
      <div className="container">
        <div className="flex flex-wrap -mx-4">
          <div className="text-white hover:text-gray-50 font-bold w-full lg:w-1/3 px-4 mb-8">
            <p className="text-sm uppercase">Lorem ipsum dolor sit</p>
            <h2 className="text-4xl mb-4">Будь з нами на зв'язку</h2>
            <div className="mb-3">
              <a className="flex items-center" href="https://facebook.com">
                <div className="w-8 h-8 mr-4"><FacebookSVG /></div>Facebook
              </a>
            </div>
            <div className="mb-3">
              <a className="flex items-center" href="https://instagram.com">
                <div className="w-8 h-8 mr-4"><InstagramSVG /></div>Instagram
              </a>
            </div>
            <div className="mb-4">
              <a className="flex items-center" href="https://twitter.com">
                <div className="w-8 h-8 mr-4"><TwitterSVG /></div>Twitter
              </a>
            </div>
            <a href="tel:+38 (011) 111 11 11">+38 (011) 111 11 11</a>
            <span className="font-light block">Прийом замовлень щодня з 9:00 до 20:00</span>
          </div>
          <div className="w-full lg:w-2/3 px-4">
            <div className="flex flex-wrap -mx-1">
              <div className="w-full sm:w-1/2 md:w-1/6 px-1 md:self-end mb-2">
                <div className="h-80 md:h-32 border-4 border-white rounded-lg overflow-hidden">
                  <img className="w-full h-full object-cover" src="https://unsplash.it/400" alt=""/>
                </div>
              </div>
              <div className="w-full sm:w-1/2 md:w-1/2 px-1 md:self-end mb-2">
                <div className="h-80 md:h-80 border-4 border-white rounded-lg overflow-hidden">
                  <img className="w-full h-full object-cover" src="https://unsplash.it/400" alt=""/>
                </div>
              </div>
              <div className="w-full sm:w-1/2 md:w-1/3 px-1 md:self-end mb-2">
                <div className="h-80 md:h-56 border-4 border-white rounded-lg overflow-hidden">
                  <img className="w-full h-full object-cover" src="https://unsplash.it/400" alt=""/>
                </div>
              </div>

              <div className="w-full sm:w-1/2 md:w-1/3 px-1 md:self-start mb-2">
                <div className="h-80 md:h-56 border-4 border-white rounded-lg overflow-hidden">
                  <img className="w-full h-full object-cover" src="https://unsplash.it/400" alt=""/>
                </div>
              </div>
              <div className="w-full sm:w-1/2 md:w-1/2 px-1 md:self-start mb-2">
                <div className="h-80 md:h-80 border-4 border-white rounded-lg overflow-hidden">
                  <img className="w-full h-full object-cover" src="https://unsplash.it/400" alt=""/>
                </div>
              </div>
              <div className="w-full sm:w-1/2 md:w-1/6 px-1 md:self-start mb-2">
                <div className="h-80 md:h-32 border-4 border-white rounded-lg overflow-hidden">
                  <img className="w-full h-full object-cover" src="https://unsplash.it/400" alt=""/>
                </div>
              </div>
              
            </div>
          </div>
        </div>
      </div>
    </div>
  )
}

export default ContactUsSection;
