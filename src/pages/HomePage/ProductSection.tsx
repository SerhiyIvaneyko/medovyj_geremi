import React from 'react';
import ProductImage1 from 'assets/images/products/product_1.jpg';
import ProductImage2 from 'assets/images/products/product_2.jpg';
import ProductImage3 from 'assets/images/products/product_3.jpg';
import ProductImage4 from 'assets/images/products/product_4.jpg';
import ProductImage5 from 'assets/images/products/product_5.jpg';
import ProductImage6 from 'assets/images/products/product_6.jpg';
import ProductImage7 from 'assets/images/products/product_7.jpg';
import ProductImage8 from 'assets/images/products/product_8.jpg';
import ProductImage9 from 'assets/images/products/product_9.jpg';
import ProductImage10 from 'assets/images/products/product_10.jpg';
import ProductImage11 from 'assets/images/products/product_11.jpg';
import ProductImage12 from 'assets/images/products/product_12.jpg';

type Props = {};

const products = [
  {
    id: 1,
    imageUrl: ProductImage1,
    title: 'Вершковий мед',
    weight: 350,
    text: 'Lorem ipsum dolor, sit amet consectetur adipisicing elit. Ducimus minima dolores dolore dolorem cupiditate asperiores explicabo eius quasi in quidem?',
    price: 20
  },{
    id: 2,
    imageUrl: ProductImage2,
    title: 'Вершковий мед',
    weight: 350,
    text: 'Lorem ipsum dolor, sit amet consectetur adipisicing elit. Ducimus minima dolores dolore dolorem cupiditate asperiores explicabo eius quasi in quidem?',
    price: 20
  },{
    id: 3,
    imageUrl: ProductImage3,
    title: 'Вершковий мед',
    weight: 350,
    text: 'Lorem ipsum dolor, sit amet consectetur adipisicing elit. Ducimus minima dolores dolore dolorem cupiditate asperiores explicabo eius quasi in quidem?',
    price: 20
  },{
    id: 4,
    imageUrl: ProductImage4,
    title: 'Вершковий мед',
    weight: 350,
    text: 'Lorem ipsum dolor, sit amet consectetur adipisicing elit. Ducimus minima dolores dolore dolorem cupiditate asperiores explicabo eius quasi in quidem?',
    price: 20
  },{
    id: 5,
    imageUrl: ProductImage5,
    title: 'Вершковий мед',
    weight: 350,
    text: 'Lorem ipsum dolor, sit amet consectetur adipisicing elit. Ducimus minima dolores dolore dolorem cupiditate asperiores explicabo eius quasi in quidem?',
    price: 20
  },{
    id: 6,
    imageUrl: ProductImage6,
    title: 'Вершковий мед',
    weight: 350,
    text: 'Lorem ipsum dolor, sit amet consectetur adipisicing elit. Ducimus minima dolores dolore dolorem cupiditate asperiores explicabo eius quasi in quidem?',
    price: 20
  },{
    id: 7,
    imageUrl: ProductImage7,
    title: 'Вершковий мед',
    weight: 350,
    text: 'Lorem ipsum dolor, sit amet consectetur adipisicing elit. Ducimus minima dolores dolore dolorem cupiditate asperiores explicabo eius quasi in quidem?',
    price: 20
  },{
    id: 8,
    imageUrl: ProductImage8,
    title: 'Вершковий мед',
    weight: 350,
    text: 'Lorem ipsum dolor, sit amet consectetur adipisicing elit. Ducimus minima dolores dolore dolorem cupiditate asperiores explicabo eius quasi in quidem?',
    price: 20
  },{
    id: 9,
    imageUrl: ProductImage9,
    title: 'Вершковий мед',
    weight: 350,
    text: 'Lorem ipsum dolor, sit amet consectetur adipisicing elit. Ducimus minima dolores dolore dolorem cupiditate asperiores explicabo eius quasi in quidem?',
    price: 20
  },{
    id: 10,
    imageUrl: ProductImage10,
    title: 'Вершковий мед',
    weight: 350,
    text: 'Lorem ipsum dolor, sit amet consectetur adipisicing elit. Ducimus minima dolores dolore dolorem cupiditate asperiores explicabo eius quasi in quidem?',
    price: 20
  },{
    id: 11,
    imageUrl: ProductImage11,
    title: 'Вершковий мед',
    weight: 350,
    text: 'Lorem ipsum dolor, sit amet consectetur adipisicing elit. Ducimus minima dolores dolore dolorem cupiditate asperiores explicabo eius quasi in quidem?',
    price: 20
  },{
    id: 12,
    imageUrl: ProductImage12,
    title: 'Вершковий мед',
    weight: 350,
    text: 'Lorem ipsum dolor, sit amet consectetur adipisicing elit. Ducimus minima dolores dolore dolorem cupiditate asperiores explicabo eius quasi in quidem?',
    price: 20
  }
]

const ProductSection:React.FC<Props> = () => {
  return (
    <div className="relative pt-16 md:pt-32 z-10">
      <p className="text-sm text-primary text-center font-bold uppercase">Lorem ipsum dolor sit</p>
      <h2 className="text-4xl font-bold text-center mb-4">Наша продукція</h2>

      <div className="px-2">
        <div className="flex flex-wrap -mx-1">
          {products.map(product => (
            <div key={`product-item-${product.id}`} className="w-full sm:w-1/2 md:w-1/3 lg:w-1/4 2xl:w-2/12 px-1 mt-2">
              <div className="card-product | relative w-full pt-full rounded-lg overflow-hidden">
                <img className="absolute top-0 left-0 w-full h-full object-cover z-10" src={product.imageUrl} alt="" />
                <div className="card-product-overlay | absolute bottom-0 left-0 w-full h-3/4 z-20 flex flex-col justify-end items-end bg-gradient-to-b from-transparent to-black p-6 text-white">
                  <div className="text-right overflow-hidden">
                    <h3 className="font-bold text-xl">{product.title}</h3>
                    <small className="block mb-2">{product.weight} грам</small>
                    <span className="text-lg font-bold">{product.price} грн</span>
                    <div className="card-product-actions">
                      <button className="
                        bg-white hover:bg-primary rounded-xl
                        text-primary hover:text-white text-md font-bold
                        py-2 px-4
                      " type="button">Замовити</button>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          ))}
        </div>
      </div>

      {/* <div className="container">
        <div className="flex flex-wrap -mx-2">
          {products.map(product => (
            <div key={`product-item-${product.id}`} className="w-full md:w-1/2 lg:w-1/3 xl:w-1/4 px-2 mt-4">
              <div className="bg-white shadow text-center rounded-lg p-8">
                <img className="inline mb-2" src="https://unsplash.it/200" alt={product.title} />
                <h3 className="block text-xl font-bold">{product.title}</h3>
                <span className="block mb-2">{product.weight}грам</span>
                <span className="block font-bold mb-4">{product.price}грн</span>
                <button className="
                  bg-primary hover:bg-primary-dark rounded-xl
                  text-white text-md font-bold
                  py-2 px-4
                " type="button">Замовити</button>
              </div>
            </div>
          ))}
        </div>
      </div> */}
    </div>
  )
}

export default ProductSection;
