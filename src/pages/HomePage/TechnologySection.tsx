import React from 'react';

type Props = {};

const technologies = [
  { id: 1, title: 'Технологія', text: 'Lorem ipsum dolor sit, amet consectetur adipisicing elit. A ipsum saepe ipsam. Veniam ea velit dolores sit aliquid expedita, maiores iure ducimus vitae culpa ipsam possimus. Consequuntur provident quis dolorum.' },
  { id: 2, title: 'Технологія', text: 'Lorem ipsum dolor sit, amet consectetur adipisicing elit. A ipsum saepe ipsam. Veniam ea velit dolores sit aliquid expedita, maiores iure ducimus vitae culpa ipsam possimus. Consequuntur provident quis dolorum.' },
  { id: 3, title: 'Технологія', text: 'Lorem ipsum dolor sit, amet consectetur adipisicing elit. A ipsum saepe ipsam. Veniam ea velit dolores sit aliquid expedita, maiores iure ducimus vitae culpa ipsam possimus. Consequuntur provident quis dolorum.' },
  { id: 4, title: 'Технологія', text: 'Lorem ipsum dolor sit, amet consectetur adipisicing elit. A ipsum saepe ipsam. Veniam ea velit dolores sit aliquid expedita, maiores iure ducimus vitae culpa ipsam possimus. Consequuntur provident quis dolorum.' }
]

const TechnologySection:React.FC<Props> = () => {
  return (
    <div className="bg-gray-100 relative py-16 md:py-32 z-10">
      <p className="text-sm text-primary text-center font-bold uppercase">Lorem ipsum dolor sit</p>
      <h2 className="text-4xl font-bold text-center mb-4">Технології які ми використовуємо</h2>

      <div className="container 2xl:w-3/5">
        <div className="flex flex-wrap -mx-2">
          {technologies.map(technology => (
            <div key={`technology-item-${technology.id}`} className="w-full md:w-1/2 px-2 mt-4">
              <div className="bg-white rounded-lg shadow p-8 text-center">
                <h3 className="text-xl font-bold mb-4">{technology.title} {technology.id}</h3>
                <p className="font-light text-lg">{technology.text}</p>
              </div>
            </div>
          ))}
        </div>
      </div>
    </div>
  )
}

export default TechnologySection;
