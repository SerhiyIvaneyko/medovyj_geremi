// import colors from 'tailwindcss/colors';

module.exports = {
  purge: ['./src/**/*.{js,jsx,ts,tsx}', './public/index.html'],
  darkMode: false, // or 'media' or 'class'
  theme: {
    container: {
      center: true,
      padding: '1rem'
    },
    fontFamily: {
      body: 'Montserrat, sans-serif',
      body2: 'Cormorant Infant, sans-serif'
    },
    extend: {
      colors: {
        primary: {
          light: 'var(--color-primary-light)',
          DEFAULT: 'var(--color-primary)',
          dark: 'var(--color-primary-dark)'
        },
        secondary: {
          light: 'var(--color-secondary-light)',
          DEFAULT: 'var(--color-secondary)',
          dark: 'var(--color-secondary-dark)'
        }
      },
      padding: {
        full: '100%'
      }
    },
  },
  variants: {
    extend: {},
  },
  plugins: [],
}
